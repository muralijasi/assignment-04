#include <stdio.h>
int main()
{
	int num, fac;
	printf("Enter the number:\n");
	scanf("%d", &num);
	printf("The factors of %d are :", num);
	
	for( fac=1 ; fac <= num ; fac++)
	{
		if(num%fac==0)
		printf("%d\n ", fac);
	}
	return 0;
}
